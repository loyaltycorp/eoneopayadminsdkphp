<?php
use EoneoPay\EoneoPay;
use EoneoPay\EoneoPayAdmin;
class TestCase extends PHPUnit_Framework_TestCase
{
    protected $eoneoEnvironment;

    /**
     * Set up the API end point and key for each test.
     */
    public function setup()
    {
        //Eoneo environment
        $this->eoneoEnvironment = 'DEV';
        if (!empty($_ENV['EONEOPAY_ENV'])) {
            $this->eoneoEnvironment = $_ENV['EONEOPAY_ENV'];
        }
        $this->eoneoEnvironment .= '_';

        //Set the API end point
        EoneoPayAdmin::setBaseUri($_ENV[$this->eoneoEnvironment . 'EONEOPAY_ENDPOINT']);

        //Set the merchant API key
        EoneoPay::setApiKey($_ENV[$this->eoneoEnvironment . 'EONEOPAY_API_KEY']);

        //Set the admin API key
        EoneoPayAdmin::setAdminApiKey($_ENV[$this->eoneoEnvironment . 'EONEOPAY_ADMIN_API_KEY']);

        //Set the API timeout
        EoneoPayAdmin::setTimeout(120.0);

        EoneoPay::setEoneoExceptions(true);
    }
}
