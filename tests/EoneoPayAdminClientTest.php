<?php

require __DIR__ . "/../vendor/autoload.php";

use EoneoPay\EoneoPay;
use EoneoPay\EoneoPayAdmin;
use EoneoPay\Customer;
use EoneoPay\Merchant;
use EoneoPay\Fee;
use EoneoPay\CreditCard;
use EoneoPay\MerchantCreditCard;
use EoneoPay\BankAccount;
use EoneoPay\MerchantBankAccount;
use EoneoPay\Payment;
use EoneoPay\Balance;
use EoneoPay\Configuration;
use EoneoPay\BankTransaction;
use EoneoPay\ClearingAccount;
use EoneoPay\PaymentAllocation;

class EoneoPayAdminClientTest extends TestCase
{
    /**
     * Test merchant create and retrieve operation.
     *
     */
    public function  testMerchantCreateAndRetrieve()
    {
        //Create a new Merchant
        $merchant = new Merchant;
        $merchant->email = 'merchant1@example.com';
        $merchant->title = 'Mr';
        $merchant->first_name = 'Test';
        $merchant->last_name = 'Merchant';
        $merchant->business_name = 'Test Merchant Pty Ltd';
        $merchant->business_website = 'http://merchant.example.com';
        $merchant->business_phone = '0123456789';
        $merchant->abn = '0123456789';
        $merchant = $merchant->save();

        $retrievedMerchant = Merchant::retrieve($merchant->id);
        $this->assertEquals($retrievedMerchant->id, $merchant->id);
        $this->assertEquals($retrievedMerchant->email, $merchant->email);
        $this->assertTrue(isset($retrievedMerchant->balance));
        $this->assertTrue(isset($retrievedMerchant->available_balance));
        $this->assertTrue(is_object($retrievedMerchant->balance));
        $this->assertTrue(is_object($retrievedMerchant->available_balance));

        $done = false;
        $found = false;
        $offset = 0;
        while (!$done && !$found) {
            $merchants = Merchant::all(['last_name' => 'Merchant'], 10, $offset++);
            $done = sizeof($merchants) == 0;
            if (!$done) {
                foreach ($merchants as $listedMerchant) {
                    $found = $listedMerchant->id == $retrievedMerchant->id;
                    if ($found) {
                        break;
                    }
                }
            }
        }
        $this->assertTrue($found);

        $retrievedMerchant->delete();
    }

    public function testMerchantSources()
    {
        //Create a new merchant
        $merchant = new Merchant;
        $merchant->email = 'merchant1@example.com';
        $merchant->title = 'Mr';
        $merchant->first_name = 'Test';
        $merchant->last_name = 'Merchant';
        $merchant->business_name = 'Test Merchant Pty Ltd';
        $merchant->business_website = 'http://merchant.example.com';
        $merchant->business_phone = '0123456789';
        $merchant->abn = '0123456789';
        $merchant = $merchant->save();

        $merchantId = $merchant->id;

        //Create a new credit card
        $creditCard = new MerchantCreditCard;
        $creditCard->number = '4444333322221111';
        $creditCard->expiry_month = 12;
        $creditCard->expiry_year = 17;
        $creditCard->name = 'Mr Example';
        $creditCard->cvc = '123';

        //Add the credit card to the customer, calls the API to store the card and obtain a token
        $merchant->addCreditCard($creditCard);

        $retrievedCreditCard = MerchantCreditCard::retrieve($creditCard->id);

        $retrievedCreditCard->name = 'Mr UpdatedExample';
        $retrievedCreditCard->save();

        //Create a new bank account
        $bankAccount = new MerchantBankAccount;
        $bankAccount->number = '012345';
        $bankAccount->bsb = '123-456';
        $bankAccount->name = 'Mr Example Account';

        //Add the bank account to the customer, calls the API to store the bank account and obtain a token
        $merchant->addBankAccount($bankAccount);

        $retrievedBankAccount = MerchantBankAccount::retrieve($bankAccount->id);

        $retrievedBankAccount->name = 'Mr UpdatedExample Account';
        $retrievedBankAccount->save();

        //Retrieve the customer through the API
        $retrievedMerchant = Merchant::retrieve($merchantId);

        //Assert the credit card PAN is correct, also confirms the credit card is stored and retrieved correctly
        $this->assertTrue($retrievedMerchant->credit_cards[0]->pan == '444433XXXXXXX111');
        $this->assertEquals("EoneoPay\\MerchantCreditCard", get_class($retrievedMerchant->credit_cards[0]));

        //Assert the bank account number is correct, also confirms the bank account is stored and retrieved correctly
        $this->assertTrue($retrievedMerchant->bank_accounts[0]->number == '012345');
        $this->assertEquals("EoneoPay\\MerchantBankAccount", get_class($retrievedMerchant->bank_accounts[0]));

        //Default payment method can no longer be deleted
        $retrievedBankAccount->delete();
        $retrievedCreditCard->delete();
        $retrievedMerchant->delete();
    }
    /**
     * Test a simple customer create and retrieve operation.
     *
     * This test also deletes the customer but does not confirm the 
     * customer is deleted through an API call. The testCreateAndDeleteCustomer 
     * test does that.
     */
    public function testCustomerCreateAndRetrieve()
    {
        //Create a new customer
        $customer = new Customer;
        $customer->first_name = 'Test1';
        $customer->last_name = 'Example';
        $customer->email = 'test1@example.com';
        $customer = $customer->save();

        //Retrieve the customer through the API
        $retrievedCustomer = Customer::retrieve($customer->id);

        //Confirm the ID and email address match
        $this->assertEquals($retrievedCustomer->id, $customer->id);
        $this->assertEquals($retrievedCustomer->email, $customer->email);

        //delete the customer
        $retrievedCustomer->delete();
    }

    /**
     * Create a new customer, update the email address and confirm the update is
     * accurate and effective.
     */
    public function testCreateAndUpdateCustomer()
    {
        //Create a new customer
        $customer = new Customer;
        $customer->first_name = 'Test2';
        $customer->last_name = 'Example';
        $customer->email = 'test2@example.com';
        $customer = $customer->save();

        //Retrieve the customer and confirm email address matches
        $retrievedCustomer = Customer::retrieve($customer->id);
        $this->assertEquals($retrievedCustomer->email, 'test2@example.com');

        //Update the email address
        $customer->email = 'test3@example.com';
        $customer = $customer->save();

        //Retrieve the customer and confirm email address matches
        $updatedCustomer = Customer::retrieve($customer->id);
        $this->assertEquals($updatedCustomer->email, 'test3@example.com');
    }

    /**
     * Create a customer and delete the customer, confirming the deletion
     * is effective by attempting to fetch the customer again through the API.
     */
    public function testCreateAndDeleteCustomer()
    {
        //Create a new customer
        $customer = new Customer;
        $customer->first_name = 'Test4';
        $customer->last_name = 'Example';
        $customer->email = 'test4@example.com';
        $customer = $customer->save();

        //Retrieve the customer through the API
        $retrievedCustomer = Customer::retrieve($customer->id);
        $this->assertEquals($retrievedCustomer->email, 'test4@example.com');

        //Delete the customer
        $retrievedCustomer->delete();

        try {
            $deletedCustomer = Customer::retrieve($retrievedCustomer->id);
            //Fail if the customer is successfull retrieved, should get a 404 response/exception
            $this->fail('Deleted customer successfully retrieved!');
        } catch (Exception $e) {
            //This is the expected response, confirm the exception is a GuzzleHttp ClientException 
            $this->assertEquals("EoneoPay\Exception\ResourceNotFoundException", get_class($e));

            //and confirm the response status code is 404
            $this->assertEquals($e->getCode(), 404);
        }
    }

    /**
     * Test credit card and bank account operations by adding one of each
     * to a new customer.
     */
    public function testCustomerSources()
    {
        //Create a new customer
        $customer = new Customer;
        $customer->first_name = 'TestSources';
        $customer->last_name = 'Example';
        $customer->email = 'test_sources@example.com';
        $customer->save();

        $customerId = $customer->id;

        //Create a new credit card
        $creditCard = new CreditCard;
        $creditCard->number = '4444333322221111';
        $creditCard->expiry_month = 12;
        $creditCard->expiry_year = 17;
        $creditCard->name = 'Mr Example';
        $creditCard->cvc = '123';

        //Add the credit card to the customer, calls the API to store the card and obtain a token
        $retrievedCreditCard = $customer->addCreditCard($creditCard);

        //Create a new bank account
        $bankAccount = new BankAccount();
        $bankAccount->number = '012345';
        $bankAccount->bsb = '123-456';
        $bankAccount->name = 'Mr Example Account';

        //Add the bank account to the customer, calls the API to store the bank account and obtain a token
        $retrievedBankAccount = $customer->addBankAccount($bankAccount);

        //Retrieve the customer through the API
        $retrievedCustomer = Customer::retrieve($customerId);

        //Assert the credit card PAN is correct, also confirms the credit card is stored and retrieved correctly
        $this->assertTrue($retrievedCustomer->credit_cards[0]->pan == '444433XXXXXXX111');

        //Assert the bank account number is correct, also confirms the bank account is stored and retrieved correctly
        $this->assertTrue($retrievedCustomer->bank_accounts[0]->number == '012345');

        //Default payment method can no longer be deleted
        //$retrievedBankAccount->delete();
        //$retrievedCreditCard->delete();
        $retrievedCustomer->delete();
    }

    /**
     * Create a credit card payment and refund it.
     */
    public function testPayments()
    {
        //Create a new customer
        $customer = new Customer;
        $customer->first_name = 'TestSources';
        $customer->last_name = 'Example';
        $customer->email = 'test_sources@example.com';
        $retrievedCustomer = $customer->save();

        //Add a credit card
        $creditCard = new CreditCard;
        $creditCard->number = '4444333322221111';
        $creditCard->expiry_month = 12;
        $creditCard->expiry_year = 17;
        $creditCard->name = 'Mr Example';
        $creditCard->cvc = '123';

        $retrievedCreditCard = $customer->addCreditCard($creditCard);

        //Create a new payment of $1, note the amount is in cents
        //Add the credit card token to the payment
        $payment = new Payment;
        $payment->amount = 100;
        $payment->token = $retrievedCreditCard->id;
        $payment->reference = 'Test payment.';
        $processedPayment = $payment->save();

        //Confirm the payment has been processed and the correct token is associated.
        $this->assertEquals($processedPayment->token, $creditCard->id);
        $this->assertEquals($processedPayment->amount, $payment->amount);

        //Refund the payment
        $refunded = $processedPayment->refund();
        $this->assertTrue($refunded);

        //Delete the credit card
        //Default payment method can no longer be deleted
        //$retrievedCreditCard->delete();

        //Delete the customer
        $retrievedCustomer->delete();
    }

    public function testBalances()
    {
        //var_dump(Balance::retrieve());
        //var_dump(Balance::history(['token' => 'tok_vO5geeIDJJvk0tZp']));
    }

    public function testFees() {
        $saveFee = false;
        try {
            $saveFee = Fee::retrieve(Fee::PAYMENT_TYPE_EFT);
        } catch (Exception $e) {
        }


        $fee = new Fee;
        $fee->payment_type = Fee::PAYMENT_TYPE_EFT;
        $fee->transaction_fee = 2.95;

        $fee->save();

        $fee = Fee::retrieve(Fee::PAYMENT_TYPE_EFT);
        $this->assertEquals($fee->payment_type, Fee::PAYMENT_TYPE_EFT);
        $this->assertEquals($fee->transaction_fee, 2.95);

        $fee->delete();

        if ($saveFee) {
            //restore the deleted fee
            $saveFee->save();
        }

        try {
            $fee = Fee::retrieve(Fee::PAYMENT_TYPE_EFT);
            $this->fail("Fee should not be found and an exception should be thrown");
        } catch (Exception $e) {
            //This is to be expected since the fee has been deleted and retrieve should throw a NotFoundException
        }

        $fee = new Fee;
        $fee->payment_type = Fee::PAYMENT_TYPE_CREDIT_CARD;
        $fee->transaction_fee = 30;
        $fee->addCardRate(Fee::CARD_TYPE_MASTERCARD, 1.5);
        $fee->addCardRate(Fee::CARD_TYPE_VISA, 1.5);
        $fee->addCardRate(Fee::CARD_TYPE_AMEX, 2.9);

        $fee->save();


        $fee = Fee::retrieve(Fee::PAYMENT_TYPE_CREDIT_CARD);
        $this->assertEquals($fee->payment_type, Fee::PAYMENT_TYPE_CREDIT_CARD);
        $this->assertEquals($fee->transaction_fee, 30);
        $this->assertEquals($fee->card_rates["MasterCard"], 1.5);
        $this->assertEquals($fee->card_rates["Visa"], 1.5);
        $this->assertEquals($fee->card_rates["AMEX"], 2.9);

        $merchant = new Merchant;
        $merchant->email = 'merchant1@example.com';
        $merchant->title = 'Mr';
        $merchant->first_name = 'Test';
        $merchant->last_name = 'Merchant';
        $merchant->business_name = 'Test Merchant Pty Ltd';
        $merchant->business_website = 'http://merchant.example.com';
        $merchant->business_phone = '0123456789';
        $merchant->abn = '0123456789';
        $merchant = $merchant->save();

        $merchantId = $merchant->id;

        $fee = new Fee;
        $fee->payment_type = Fee::PAYMENT_TYPE_CREDIT_CARD;
        $fee->transaction_fee = 30;
        $fee->merchant_id = $merchantId;
        $fee->addCardRate(Fee::CARD_TYPE_MASTERCARD, 1.5);
        $fee->addCardRate(Fee::CARD_TYPE_VISA, 1.5);
        $fee->addCardRate(Fee::CARD_TYPE_AMEX, 2.9);

        $fee->save();

        $retrievedFee = Fee::retrieve(Fee::PAYMENT_TYPE_CREDIT_CARD, $merchantId);
        $this->assertEquals($merchantId, $retrievedFee->merchant_id);
        $this->assertEquals(30, $retrievedFee->transaction_fee);

        $retrievedFee->delete();

        $fee = Fee::retrieve(Fee::PAYMENT_TYPE_CREDIT_CARD);
        $this->assertEquals($fee->payment_type, Fee::PAYMENT_TYPE_CREDIT_CARD);
    }

    public function testConfiguration() {
        $configuration = Configuration::retrieve("merchant_settlement_period");
        $this->assertEquals("merchant_settlement_period", $configuration->key);

        $current_value = $configuration->value;

        $configuration->value = $current_value * 2;
        $configuration->save();

        $configuration = Configuration::retrieve("merchant_settlement_period");
        $this->assertEquals("merchant_settlement_period", $configuration->key);
        $this->assertEquals($current_value * 2, $configuration->value);

        $configuration->value = $current_value;
        $configuration->save();

        $configuration = Configuration::retrieve("merchant_settlement_period");
        $this->assertEquals("merchant_settlement_period", $configuration->key);
        $this->assertEquals($current_value, $configuration->value);
    }

    public function testPaymentDateFilter()
    {
        ///v1/payments?filter%5B0%5D=reference_number&filter%5B1%5D=sub_69c86a150d5a558c&filter%5B2%5D%5B0%5D=txn_date&filter%5B2%5D%5B1%5D=%3E%3D&filter%5B2%5D%5B2%5D=2016-08-29+00%3A00%3A00&filter%5B3%5D%5B0%5D=txn_date&filter%5B3%5D%5B1%5D=%3C%3D&filter%5B3%5D%5B2%5D=2016-09-05+23%3A59%3A59
        $payments = Payment::all([["reference_number", "=", "sub_69c86a150d5a558c"], ["txn_date", ">=", "2016-08-29 00:00:00"], ["txn_date", "<=", "2016-09-26 02:08:59"]]);
        //$payments = Payment::all([["reference_number", "=", "sub_69c86a150d5a558c"]]);
    }

    public function testBankTransactions()
    {
        /*$bankTransaction = BankTransaction::retrieve("20161222-1");
        var_dump($bankTransaction);*/
        //$bankTransactions = BankTransaction::all([["txn_date", ">=", "2016-12-20"]]);
        //var_dump($bankTransactions);
    }

    public function testClearingAccounts()
    {
        $clearingAccounts = ClearingAccount::all();
        var_dump($clearingAccounts);

        $clearingAccount = ClearingAccount::retrieve(3);
        var_dump($clearingAccount);
        /*$merchant = Merchant::retrieve('mer_f9a1f8939d6a1d83');
        var_dump($merchant);*/
    }

    public function testPaymentAllocations()
    {
        $paymentAllocations = PaymentAllocation::all([["allocation_type", "=", "App\\Models\\EWallet"], ["allocation_date", ">=", "2017-01-31 00:00:00"]]);
        var_dump($paymentAllocations);
    }

    public function testCanUserSearchForMerchants()
    {
        $random = rand(100000,999999);
        $merchant = new Merchant;
        $merchant->email = 'merchant1@example.com';
        $merchant->title = 'Mr';
        $merchant->first_name = 'Test';
        $merchant->last_name = 'Merchant';
        $merchant->business_name =  $random . 'TestMerchant';
        $merchant->business_website = 'http://merchant.example.com';
        $merchant->business_phone = '0123456789';
        $merchant->abn = '0123456789';
        $merchant = $merchant->save();

        $searchResults = EoneoPayAdmin::search($random, [Merchant::class]);

        $this->assertCount(1, $searchResults);
        $this->assertEquals(get_class($searchResults[0]), Merchant::class);
        $this->assertEquals($searchResults[0]->id, $merchant->id);
    }

    /*public function testHarcourtsAddAccount()
    {
        $merchant = Merchant::retrieve("mer_dCnfd1QrU1Ex9SNa");

        //Create a new bank account
        $bankAccount = new MerchantBankAccount;
        $bankAccount->number = '986819548';
        $bankAccount->bsb = '183-170';
        $bankAccount->name = 'Casey Gold Estate Agents Pty Ltd Tr';

        $merchant->addBankAccount($bankAccount);
    }*/

    /*public function testDeleteAccountErrors()
    {
        $oOwner = Merchant::retrieve(mer_JvXUKMUY3e30ptSL);


        foreach($oOwner->bank_accounts as $oAccount) {
            if($oAccount->id == 'src_13382039ad68a9db') {
                $oAccount->delete();
                return true;
            }
        }
    }*/
}
