<?php

require __DIR__ . "/../vendor/autoload.php";
use EoneoPay\TestTransaction;
use EoneoPay\BankTransaction;
use EoneoPay\Customer;
use EoneoPay\Plan;
use EoneoPay\Subscription;

class BankTransactionTest extends TestCase
{
    public function testCanUserAllocateBankTransaction()
    {
        //Create a new customer
        $customer = new Customer;
        $customer->first_name = 'Test1';
        $customer->last_name = 'Example';
        $customer->email = 'test1@example.com';
        $customer = $customer->save();

        //Retrieve the customer through the API
        $retrievedCustomer = Customer::retrieve($customer->id);

        $plan = new Plan;
        $plan->name = 'Test Plan';
        $plan->amount = 100000;
        $plan->interval = Plan::INTERVAL_MONTH;
        $plan->interval_count = 2;
        $plan = $plan->save();

        //Create the subscription to manually allocate to
        $subscription = new Subscription;
        $subscription->plan = $plan->id;
        $subscription->start_date = date("Y-m-d");
        $subscription->end_date = strtotime("+1 year", time());
        $newSubscription = $retrievedCustomer->addSubscription($subscription);

        //create the test transfer which does not have any reference to the subscription in the text
        $testTransaction = new TestTransaction;
        $testTransaction->amount = (int) rand(1000,9999);
        $testTransaction->code = '920';
        $testTransaction->text = "    SHIIIT TESTER MELBOURNE";
        $testTransaction->file_name = time();
        $testTransaction->save();

        //Get all bank transactions - filter on amount which is a random number
        $retrievedBankTransaction = BankTransaction::all([['amount', '=', $testTransaction->amount]])[0];
        $this->assertEquals($retrievedBankTransaction->amount, $testTransaction->amount);

        //Allocate using the endpoint
        $retrievedBankTransaction->allocate($newSubscription);

        //Get the subscription again to verify that is has been allocated
        $retrievedSubscription = Subscription::retrieve($subscription->id);
        $this->assertEquals($retrievedSubscription->current_balance, $plan->amount - $testTransaction->amount);
    }
}
