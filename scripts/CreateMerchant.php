<?php

require __DIR__ . "/../vendor/autoload.php";

use EoneoPay\EoneoPayAdmin;
use EoneoPay\Customer;
use EoneoPay\Merchant;
use EoneoPay\Fee;
use EoneoPay\CreditCard;
use EoneoPay\MerchantCreditCard;
use EoneoPay\BankAccount;
use EoneoPay\MerchantBankAccount;
use EoneoPay\Payment;
use EoneoPay\Balance;
use EoneoPay\Configuration;

//EoneoPayAdmin::setBaseUri("http://localhost:2200");
//EoneoPayAdmin::setBaseUri("http://api-staging.eoneopay.com");
EoneoPayAdmin::setBaseUri("https://api.eoneopay.com");
//EoneoPayAdmin::setAdminApiKey("sk_test_ff98e683331743a8e4e68e7f");
EoneoPayAdmin::setAdminApiKey("sk_live_f7be5d6f49fffb4a2f33738b");
EoneoPayAdmin::setTimeout(120.0);

//Create a new Merchant
$merchant = new Merchant;
$merchant->email = '';
$merchant->title = '';
$merchant->first_name = 'Remarkable';
$merchant->last_name = 'Woman';
$merchant->business_name = 'The Remarkable Woman';
$merchant->business_website = '';
$merchant->business_phone = '';
$merchant->abn = '';
$merchant->clearing_account = 1; //rewards pay
//$merchant->clearing_account = 2; //pay rent rewards
//$merchant->clearing_account = 3; //member rewards
//$merchant->clearing_account = 4; //loyalty corp holding
//$merchant->clearing_account = 5; //star cash
$merchant = $merchant->save();
var_dump($merchant);

