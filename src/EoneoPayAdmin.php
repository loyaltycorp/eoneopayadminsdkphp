<?php

namespace EoneoPay;

class EoneoPayAdmin extends EoneoPay
{
    /**
     * The API key to use for admin only end points.
     */
    static protected $adminApiKey;

    /**
     * Set the Admin API key to use for requests to the admin API functions
     *
     * @param string $apiKey The Admin API key
     */
    static public function setAdminApiKey($apiKey)
    {
        static::$adminApiKey = $apiKey;
    }

    static public function setApiKey($apiKey)
    {
        throw new \Exception("Do not set the merchant API key using this method. Please call EoneoPay::setApiKey() to set the merchant API key. Call EoneoPayAdmin::setAdminApiKey() to set the admin API key.");
    }

    /**
     * Send an HTTP request to the API end point.
     *
     * @param string $method The method to use for the request
     * @param string $endPoint The request end point.
     * @param array $data The data to include with the request
     * @return A HTTP response with headers, body, etc
     */
    static public function makeRequest($method, $endPoint, $data = null, $key = null)
    {
        $apiKey = static::$apiKey;
        $response = parent::makeRequest($method, $endPoint, $data, static::$adminApiKey);
        return $response;
    }

    /**
     * Search and return Eoneo objects
     *
     * @param string $query The string to search for within the scope
     * @param array $scope An array of fully qualified class names to scope the query on
     * @return An array of Eoneo objects inheriting from the Resource object
     */
    static public function search($query, $scope = [])
    {
        $scopeTranslation = [
            Merchant::class => 'merchants',
            Customer::class => 'customers',
            Payment::class => 'payments',
            Plan::class    => 'plans',
        ];
        $queryScope = [];
        foreach ($scope as $scopeItem) {
            if (isset($scopeTranslation[$scopeItem])) {
                $queryScope[] = $scopeTranslation[$scopeItem];
            }
        }
        if (!count($queryScope)) {
            $queryScope = array_values($scopeTranslation);
        }
        $searchResults = [];
        $response = parent::makeRequest('GET', sprintf('search?q=%s&s=%s', urlencode($query), implode(',', $queryScope)));
        $body = json_decode($response->getBody());
        if (empty($body->list)) {
            return [];
        }
        if (!empty($body->list->merchants)) {
            foreach ($body->list->merchants as $object) {
                $merchant = new Merchant;
                foreach (get_object_vars($object) as $name => $value) {
                    $merchant->$name = $value;
                }
                $searchResults[] = $merchant;
            }
        }
        if (!empty($body->list->customers)) {
            foreach ($body->list->customers as $object) {
                $customer = new Customer;
                foreach (get_object_vars($object) as $name => $value) {
                    $customer->$name = $value;
                }
                $searchResults[] = $customer;
            }
        }
        if (!empty($body->list->payments)) {
            foreach ($body->list->payments as $object) {
                $payment = new Payment;
                foreach (get_object_vars($object) as $name => $value) {
                    $payment->$name = $value;
                }
                $searchResults[] = $payment;
            }
        }
        if (!empty($body->list->plans)) {
            foreach ($body->list->plans as $object) {
                $plan = new Plan;
                foreach (get_object_vars($object) as $name => $value) {
                    $plan->$name = $value;
                }
                $searchResults[] = $plan;
            }
        }
        return $searchResults;
    }
}
