<?php

namespace EoneoPay;

trait AdminResourceTrait
{
    static public function makeRequest($method, $endPoint, $data = null)
    {
        return EoneoPayAdmin::makeRequest($method, $endPoint, $data);
    }
}
