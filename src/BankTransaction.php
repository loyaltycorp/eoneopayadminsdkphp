<?php

namespace EoneoPay;

/**
 * Class to query bank transactions.
 */
class BankTransaction extends AdminResource
{
    use AdminResourceTrait;

	static function __init__()
    {
        EoneoPay::registerEoneoException('400', '10000', 'EoneoPay\Exception\EoneoValidationException');
        EoneoPay::registerEoneoException('404', '10100', 'EoneoPay\Exception\ResourceNotFoundException');
        EoneoPay::registerEoneoException('404', '10101', 'EoneoPay\Exception\ResourceNotFoundException');
        EoneoPay::registerEoneoException('404', '10102', 'EoneoPay\Exception\ResourceNotFoundException');
	}

    static protected function getIdProperty()
    {       
        return "id"; 
    }   
        
    static protected function getEndPoint($instance = null)
    {
        return "bankTransactions";
    }

    static protected function getRequiredProperties()
    {
        return [];
    }

    public function allocate(Resource $feeable = null)
    {
        $allocationType = null;
        switch (get_class($feeable)) {
            case Subscription::class:
                $allocationType = 'subscription';
                break;
            case Ewallet::class:
                $allocationType = 'ewallet';
                break;
            case Charge::class:
                $allocationType = 'charge';
                break;  
            default:
                throw new \Exception('Resource not recognised as a feeable (subscription|ewallet|charge)');
        }
        $idProperty = $feeable->getIdProperty();
        $request = [
            'allocation_type' => $allocationType,
            'allocation_id'   => $feeable->$idProperty,
        ];

        $response = EoneoPayAdmin::makeRequest(EoneoPay::POST, static::getEndPoint() . "/" . $this->id . "/allocate", $request);
        if ($response->getStatusCode() == 200) {
            return json_decode($response->getBody());
        }
    }
}

BankTransaction::__init__();
