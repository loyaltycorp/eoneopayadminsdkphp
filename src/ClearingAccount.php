<?php

namespace EoneoPay;

/**
 * Class to query bank transactions.
 */
class ClearingAccount extends AdminResource
{
    use AdminResourceTrait;

    static protected function getIdProperty()
    {       
        return "id"; 
    }   
        
    static protected function getEndPoint($instance = null)
    {
        return "clearingAccounts";
    }

    static protected function getRequiredProperties()
    {
        return [];
    }
}
