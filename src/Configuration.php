<?php

namespace EoneoPay;

/**
 * Class to create and manage merchants.
 */
class Configuration extends AdminResource
{
	static function __init__()
    {
        EoneoPay::registerEoneoException('400', '11000', 'EoneoPay\Exception\EoneoValidationException');
        EoneoPay::registerEoneoException('404', '11100', 'EoneoPay\Exception\ResourceNotFoundException');
	}

    static protected function getIdProperty()
    {       
        return "key"; 
    }   
        
    static protected function getEndPoint($instance = null)
    {
        return "configuration";
    }

    static protected function getRequiredProperties()
    {
        return ['value'];
    }
}

Configuration::__init__();
