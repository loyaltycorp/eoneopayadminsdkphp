<?php

namespace EoneoPay;

/**
 * Class to create and manage fees.
 */
class Fee extends AdminResource
{
    /**
     * Valid payment types
     */
    const PAYMENT_TYPE_CREDIT_CARD = 'cc_payment';
    const PAYMENT_TYPE_DIRECT_DEBIT = 'direct_debit';
    const PAYMENT_TYPE_BPAY = 'bpay';
    const PAYMENT_TYPE_EFT = 'eft';
    const PAYMENT_TYPE_AUSPOST = 'auspost';

    const CARD_TYPE_MASTERCARD = "MasterCard";
    const CARD_TYPE_VISA = "Visa";
    const CARD_TYPE_AMEX = "AMEX";

    public $merchant_id = "*";
    public $card_rates = [];

	static function __init__()
    {
        EoneoPay::registerEoneoException('400', '12000', 'EoneoPay\Exception\EoneoValidationException');
        EoneoPay::registerEoneoException('404', '12100', 'EoneoPay\Exception\ResourceNotFoundException');
        EoneoPay::registerEoneoException('404', '12101', 'EoneoPay\Exception\ResourceNotFoundException');
	}

    static protected function getIdProperty()
    {       
        return "payment_type"; 
    }   
        
    static protected function getEndPoint($instance = null)
    {
        return "fees";
    }

    static protected function getRequiredProperties()
    {
        return ['merchant_id', 'transaction_fee'];
    }

    public function addCardRate($cardType, $rate)
    {
        $this->card_rates[$cardType] = $rate;
    }

    static protected function getObjectForResponseValue($responseValue)
    {
        $object = [];

        foreach (get_object_vars($responseValue) as $name => $value) {
            $object[$name] = $value;
        }

        return $object;
    }

    static public function retrieve($id = 'self', $merchantId = false)
    {
        $response = static::makeRequest(EoneoPay::GET, static::getEndPoint() . "/" . $id . ($merchantId ? "?merchant_id=$merchantId" : ""));
        if ($response->getStatusCode() == 200) {
            return static::getObjectFromResponse($response, null, true);
        }

        return null;
    }

	/**
     * Delete an instance of this resource.
     */
    public function delete($returnResponse = false)
    {
        $response = static::makeRequest(EoneoPay::DELETE, static::getEndPoint($this) . "/" . $this->getId() . ($this->merchant_id != "*" ? "?merchant_id=$this->merchant_id" : ""));
        if ($returnResponse && $response->getStatusCode() == 200) {
            return static::getObjectFromResponse($response, null, true);
        }

        return $response->getStatusCode() == 200;
    }
}

Fee::__init__();
