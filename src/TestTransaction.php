<?php

namespace EoneoPay;

/**
 * Class to query bank transactions.
 */
class TestTransaction extends AdminResource
{
    use AdminResourceTrait;

    static protected function getIdProperty()
    {       
        return "reference_number"; 
    }   
        
    static protected function getEndPoint($instance = null)
    {
        return "testAccountInformation";
    }

    static protected function getRequiredProperties()
    {
        return [];
    }
}
