<?php

namespace EoneoPay;

/**
 * Class to query payment allocations
 */
class PaymentAllocation extends AdminResource
{
    use AdminResourceTrait;

    static protected function getIdProperty()
    {       
        return "id"; 
    }   
        
    static protected function getEndPoint($instance = null)
    {
        return "paymentAllocations";
    }

    static protected function getRequiredProperties()
    {
        return [];
    }
}
