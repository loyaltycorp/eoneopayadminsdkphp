<?php

namespace EoneoPay;

/**
 * Class to create and manage merchants.
 */
class Merchant extends Profile
{
    use AdminResourceTrait;

	static function __init__()
    {
        //CardHolder exceptions
        EoneoPay::registerEoneoException('400', '3000', 'EoneoPay\Exception\EoneoValidationException');
        EoneoPay::registerEoneoException('400', '3002', 'EoneoPay\Exception\InalidPaymentSourceException');
        EoneoPay::registerEoneoException('400', '3009', 'EoneoPay\Exception\InvalidPaymentSourceException');
        EoneoPay::registerEoneoException('500', '3010', 'EoneoPay\Exception\TokenisationFailedException');
        EoneoPay::registerEoneoException('404', '3100', 'EoneoPay\Exception\ResourceNotFoundException');
        EoneoPay::registerEoneoException('400', '3300', 'EoneoPay\Exception\DeleteDefaultPaymentSourceException');
    
        //Reference number allocation exceptions
        EoneoPay::registerEoneoException('400', '2200', 'EoneoPay\Exception\ResourceNotFoundException');
        EoneoPay::registerEoneoException('409', '2201', 'EoneoPay\Exception\ResourceNotFoundException');
        EoneoPay::registerEoneoException('400', '2210', 'EoneoPay\Exception\ResourceNotFoundException');
        EoneoPay::registerEoneoException('409', '2211', 'EoneoPay\Exception\ResourceNotFoundException');
    }   

    static protected function getObjectForResponseValue($responseValue)
    {
        $object = null;

        if (property_exists($responseValue, 'pan')) {
            $object = new MerchantCreditCard;
        } else if (property_exists($responseValue, 'bsb')) {
            $object = new MerchantBankAccount;
        } else {
            $object = new \stdClass;
        }

        if ($object) {
            foreach (get_object_vars($responseValue) as $name => $value) {
                $object->$name = $value;
            }
        }

        return $object;
    }
}

Merchant::__init__();
