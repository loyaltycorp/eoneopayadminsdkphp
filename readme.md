## EoneoPay Payment Gateway API Client

Eoneo payment gateway API Client for admin only end points.

Admin only end points include merchant and API key access. To manage merchants through the API the Admin client is needed along with an Admin API key.

## Documentation

# Setup
1. Composer

Add the following entries to composer.json:
```
"repositories": [
    {
        "type": "vcs",
        "url": "https://git@bitbucket.org/loyaltycorp/eoneopayadminclientlibrary_php.git"
    }
],
"require": {
    "eoneopay/adminclient": "dev-master"
}
```

Run composer install
```shell
composer install
```

2. Use the EoneoPay API client:
```php
use EoneoPay\EoneoPayAdmin;
```

3. The library assumes the base API URI is https://api.eoneopay.com. To set a different base URI:
```php
EoneoPay::setBaseUri('https://api.eoneopay.com');
```

4. Set the API key
```php
EoneoPay::setAdminApiKey('sk_test_someadminapikey');
//If using the full API set the merchant API Key as well
EoneoPay::setApiKey('sk_test_somemerchantapikey');
```

# Create a merchant
To create merchant instantiate a Merchant, set properties and save:
```php
//use EoneoPay\Merchant;
$merchant = new Merchant;
$merchant->email = 'merchant1@example.com';
$merchant->title = 'Mr';
$merchant->first_name = 'Test';
$merchant->last_name = 'Merchant';
$merchant->business_name = 'Test Merchant Pty Ltd';
$merchant->business_website = 'http://merchant.example.com';
$merchant->business_phone = '0123456789';
$merchant->abn = '0123456789';
$merchant = $merchant->save();

//A specific clearing account can be set on the merchant
//This would be required where payments processed for this merchant should be deposited into an account other than the general payment gateway account
//The value for the clearing account is the internal clearing account ID which must be provided by a payment gateway admin
//$merchant->clearing_account = 3

//For merchants with trust accounts a trust clearing account should be set
//The trust clearing account is used when transferring funds to the merchant's trust account, see the comment in adding a bank account below
//The value for the trust clearing account is the internal clearing account ID which must be provided by a payment gateway admin
//$merchant->trust_clearing_account = 6
```

# Retrieve a merchant
Get a valid merchant ID and call Merchant::retrieve()
```php
$id = 'mer_1234567890';
$retrievedMerchant = Merchant::retrieve($id);
```

# Get a list of merchants
To get a list of merchants
```php
$merchants = Merchant::all(['last_name' => 'Merchant'], 10, 0); //Get the first page of 10 merchants with last name 'Merchant'
```

# Update a merchant
Retrieve a merchant, change properties and call Merchant::save()
```php
$id = 'mer_1234567890';
$merchant = Merchant::retrieve($id);
$merchant->email = 'test3@example.com';
$merchant = $merchant->save();
```

# Delete a merchant
Retrieve a merchant through the API and then call Merchant::delete()
```php
$merchant->delete();
```
# Add a credit card
Get a merchant and add a credit card
```php
$creditCard = new MerchantCreditCard;
$creditCard->number = '123456789012';
$creditCard->expiry_month = 12;
$creditCard->expiry_year = 17;
$creditCard->name = 'Mr Example';
$creditCard->cvc = '123';

$id = 'mer_1234567890';
$merchant = Merchant::retrieve($id);
$addedCreditCard = $merchant->addCreditCard($creditCard);

//To remove the credit card
$addedCreditCard->delete();
```

# Add a bank account
Get a merchant and add a bank account
```php
$bankAccount = new MerchantBankAccount();
$bankAccount->number = '012345';
$bankAccount->bsb = '123-456';
$bankAccount->name = 'Mr Example Account';

//For trust accounts set the trust flag to true
//Transfers from trust accounts are handled differently than transfers from non-trust accounts.
//Trust account transfers are sent first to the trust clearing account
//Once the funds have been disbursed a second transfer is created to move the funds from the trust clearing account
//to the merchant's trust account
//$bankAccount->trust = true;


$id = 'mer_1234567890';
$merchant = Merchant::retrieve($id);
$addedBankAccount = $merchant->addBankAccount($bankAccount);

//To remove the bank account
$addedBankAccount->delete();
```

# Create fee for payment type
```php
$fee = new Fee;
$fee->payment_type = Fee::PAYMENT_TYPE_EFT;
$fee->transaction_fee = 2.95;
$fee->save();
```

# Retrieve fees for payment types
```php
$fee = Fee::retrieve(Fee::PAYMENT_TYPE_CREDIT_CARD);
$fee = Fee::retrieve(Fee::PAYMENT_TYPE_EFT);
```

# Delete a fee
```php
$fee = Fee::retrieve(Fee::PAYMENT_TYPE_EFT);
$fee->delete();
```

# Bank reports
```php
//Get a list of clearing accounts
$clearingAccounts = ClearingAccount::all();

//Get a list of bank transactions
$bankTransactions = BankTransaction::all([["txn_date", ">=", "2016-10-15"]]);

//Get a list of payment allocations
//This example gets all EWallet allocations (funds credited to ewallet balances)
$paymentAllocations = PaymentAllocation::all([["allocation_type", "=", "EWallet"], ["allocation_date", ">=", "2016-11-19 00:00:00"]]);
```
